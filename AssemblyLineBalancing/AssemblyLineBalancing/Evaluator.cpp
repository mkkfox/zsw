#include "Evaluator.h"

float Evaluator::calculateLE(int * tt, int ct, int sc)
{
	float temp = (float)ct*sc;
	for (int i = 0; i < sc; i++) temp -= tt[i];
	temp = (float)temp / (float)(ct*sc) * (float)100.0;
	return temp;
}

float Evaluator::calculateSI(int * tt, int ct, int sc)
{
	float average = 0;
	float temp = 0;
	for (int i = 0; i < sc; i++) temp += tt[i] ;
	average = (ct*sc - temp) / (float)sc;
	temp = 0;
	for (int i = 0; i < sc; i++) temp += pow(average - (ct - tt[i]), 2);
	temp = (float)2.0 * temp;
	temp = sqrt(temp);
	return temp;
}

float Evaluator::calculateT(int * tt, int ct, int sc)
{
	return (float)ct*sc - tt[sc - 1];
}

Evaluator::Evaluator()
{
	LE = -1;
	SI = -1;
	T = -1;
	data = "";
}

Evaluator::~Evaluator()
{
}

int Evaluator::print(std::ostream & target) const
{
	target << data;
	return 0;
}

std::string Evaluator::evaluate(int * timetable, int cycletime, int stationcount)
{
	LE = calculateLE(timetable, cycletime, stationcount);
	SI = calculateSI(timetable, cycletime, stationcount);
	T = calculateT(timetable, cycletime, stationcount);
	std::stringstream ss;
	ss.precision(4);
	ss << "LE = " << LE << " SI = " << SI << " T = " << T << std::endl;
	data = ss.str();
	return data;
}
