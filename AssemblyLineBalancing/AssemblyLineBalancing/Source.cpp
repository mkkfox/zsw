#include "Solver.h"

int main(int argc, char *argv[])
{
	Solver prototype;
	
	if (argc > 2)
	{
		int x = atoi(argv[2]);
		if (x < 1) x = 1;
		std::string file = argv[1];
		std::cout << file << " :  " << argv[2] <<std::endl;
		prototype.solve(file, x);
	}
	else
	{
		std::cout << "test.txt :  2" << std::endl;
		prototype.solve("test.txt", 2);
	}
	prototype.show();
	prototype.save("Output.txt");

	std::cout << std::endl;
	system("pause");
}