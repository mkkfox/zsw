#pragma once
#include "Printable.h"
#include <sstream>

class Evaluator 
	:public Printable
{
private:
	float calculateLE(int * tt, int ct, int sc);
	float calculateSI(int * tt, int ct, int sc);
	float calculateT(int * tt, int ct, int sc);
protected:
	float LE;
	float SI;
	float T;
	std::string data;
public:
	Evaluator();
	~Evaluator();
	int print(std::ostream &target) const override;
	std::string evaluate(int *timetable, int cycletime, int stationcount);
};

