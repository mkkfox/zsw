#pragma once
#include <list>
#include <fstream>
#include <iostream>
#include "Printable.h"


class FileHandler
{
public:
	FileHandler();
	~FileHandler();
	std::list<std::string> loadData(const std::string filename);
	int saveData(const std::string filename, const Printable &data) const;
	int newFile(const std::string filename) const;
};

