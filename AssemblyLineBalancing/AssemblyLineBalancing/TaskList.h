#pragma once
#include "Task.h"
#include <sstream>
#include "Printable.h"

bool compareP(const Task &L, const Task &R);
bool compareI(const Task &L, const Task &R);

class TaskList
	:public Printable
{
public:
	TaskList();
	~TaskList();
	std::list<Task> tasks;
	int *locktable;
private:
	int calculate();
public: 
	int buildList(std::list<std::string> inputdata);
	int print(std::ostream &target) const override;
	int sortPrio();
	int sortID();
};

