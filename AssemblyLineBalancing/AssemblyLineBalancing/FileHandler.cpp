#include "FileHandler.h"

FileHandler::FileHandler()
{
}

FileHandler::~FileHandler()
{
}

std::list<std::string> FileHandler::loadData(const std::string filename)
{
	std::fstream input;
	input.open(filename, std::fstream::in);
	if (!input.is_open()) 
	{
		std::cout << "Failed opening file" << std::endl; 
		system("pause");
		return std::list<std::string>();
	}
	std::list<std::string> data;
	char temp[100];
	while (!input.eof())
	{
		input.getline(temp, 100);
		data.push_back(temp);
	}
	input.close();
	return data;
}

int FileHandler::saveData(const std::string filename, const Printable & data) const
{
	std::fstream output;
	output.open(filename, std::fstream::out | std::fstream::app | std::fstream::ate);
	if (output.fail()) return -1;
	data.print(output);
	output.close();
	return 0;
}

int FileHandler::newFile(const std::string filename) const
{
	std::fstream file;
	file.open(filename, std::fstream::out | std::fstream::trunc);
	return 0;
}
