#pragma once
#include <iostream>
//typedef int(*printFunction)(std::ostream &);
class Printable
{
protected:
	Printable();
public:
	virtual ~Printable();

	virtual int print(std::ostream &target) const = 0;
};

