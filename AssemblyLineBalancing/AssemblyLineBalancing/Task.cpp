#include "Task.h"

int Task::count = 1;

Task::Task(unsigned int value, std::list<int> successors)
{
	ID = count;
	count++;
	this->value = value;
	this->successors = successors;
	this->importance = -1;
}


Task::~Task()
{
	successors.clear();
}

int Task::getID() const
{
	return ID;
}

int Task::getValue() const
{
	return value;
}

int Task::getPrio() const
{
	return importance;
}

int Task::setImportance(const int val)
{
	return importance = val;
}

int Task::print(std::ostream &target) const
{
	std::stringstream ss;
	
	ss << "ID:\t" << ID << " \tTime:\t" << value << "\tSuccessors: ";
	for (int val : successors)
	{
		ss << val;
		ss << " ";
	}
	ss << "\t\tPriority: " << importance;
	target << ss.str() << std::endl;
	return 0;
}
