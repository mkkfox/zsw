#pragma once
#include "TaskList.h"
#include "FileHandler.h"
#include "Evaluator.h"

class Solver
	:public Printable
{
public:
	Solver();
	~Solver();
protected:
	TaskList List;
	FileHandler FH;
	Evaluator Ev;

	int getOptimal();
	int cyclevalue;
	int optimal;
	int minimal;
	int stations;

	int createLine();
	int *timetable;
	std::list<std::string> solution;
	int print(std::ostream &target) const override;
public:
	int show();
	int solve(const std::string filename);
	int solve(const std::string filename, const int stationcount);
	int save(const std::string filename) const;
};

