#pragma once
#include <list>
#include <iostream>
#include <sstream>
#include "Printable.h"

class Task
	:public Printable
{
public:
	Task(unsigned int value, std::list<int> successors);
	~Task();

	static int count;
protected:
	// Unique Task ID
	int ID;
	// Task lenght
	int value;
	// Task priority importance
	int importance;
public:
	// Dependent task's IDs
	std::list<int> successors;

	int getID() const;
	int getValue() const;
	int getPrio() const;

	int setImportance(const int val);
	int print(std::ostream &target) const override;
};

