#include "TaskList.h"

TaskList::TaskList()
{
}

TaskList::~TaskList()
{
	delete[] locktable;
	tasks.clear();
}

bool compareP(const Task &L, const Task &R)
{
	return L.getPrio() > R.getPrio();
}

bool compareI(const Task &L, const Task &R)
{
	return L.getID() < R.getID();
}

int TaskList::calculate()
{ 
	int val;
	int *valuetable = nullptr;
	int i = tasks.size();
	valuetable = new int[i--];
	
	for (std::list<Task>::reverse_iterator navi = tasks.rbegin(); navi != tasks.rend(); ++navi)
	{
		val = navi->getValue();
		switch (navi->successors.size())
		{
		case 0: // No successors
			break;
		case 1:	// Single successor
			val += valuetable[navi->successors.front()-1];
			locktable[navi->successors.front() - 1]++;
			break;
		default: // Multiple successors
			int temp = 0;
			for (std::list<int>::iterator iter = navi->successors.begin(); iter != navi->successors.end(); iter++)
			{
				if (valuetable[*iter-1] > temp) temp = valuetable[*iter-1];
				locktable[*iter-1]++;;
			}
			val += temp;
			break;
		}

		navi->setImportance(val);
		valuetable[i--] = val;
	}	
	
	if (valuetable != nullptr) delete[] valuetable;
	return 0;
}

int TaskList::buildList(std::list<std::string> inputdata)
{
	std::stringstream line;
	int value;
	int temp;
	std::list<int> following;
	int *valtab;
	int i = 0;

	valtab = new int[inputdata.size()];
	locktable = new int[inputdata.size()];
	for (int i = 0; i < (int)inputdata.size(); i++) locktable[i] = 0;

	while (!inputdata.empty())
	{
		line.str(inputdata.front());
		line >> value;
		valtab[i++] = value;
		while (line >> temp) following.push_back(temp);
		tasks.push_back(Task(value, following));
		following.clear();
		inputdata.pop_front();
		line.clear();
	}
	return calculate();
}

int TaskList::print(std::ostream &target) const
{
	for (Task a : tasks) a.print(target);
	target << std::endl;
	return 0;
}

int TaskList::sortPrio()
{
	tasks.sort(compareP);
	return 0;
}

int TaskList::sortID()
{
	tasks.sort(compareI);
	return 0;
}
