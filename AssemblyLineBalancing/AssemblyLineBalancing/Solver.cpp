#include "Solver.h"



Solver::Solver()
{
	cyclevalue = 0;
	minimal = 0;
	optimal = 0;
	stations = 2;
	timetable = nullptr;
}


Solver::~Solver()
{
	delete[] timetable;
}


int Solver::getOptimal()
{
	int a;
	for (Task var : List.tasks)
	{
		optimal += var.getValue();
		if (minimal < var.getValue()) minimal = var.getValue();
	}
	a = optimal % stations;
	if (a) optimal = (optimal - a) / stations + 1;
	else optimal = optimal / stations;
	if (minimal > optimal) cyclevalue = minimal;
	else cyclevalue = optimal;
	return optimal;
}

int Solver::createLine()
{
	List.sortPrio();

	bool *usedtable;
	int count = List.tasks.size();
	usedtable = new bool[count];
	timetable = new int[stations];
	int* t_lock;
	t_lock = new int[count];
	for (bool solved = 0; !solved; cyclevalue++)
	{
		for (int i = 0; i < count; i++)
		{
			usedtable[i] = 0;
			t_lock[i] = List.locktable[i];
		}
		
		std::stringstream ss;
		std::list<Task>::iterator navi;

		for (int i = 0, j = 0; i < stations; i++, j = 0)
		{
			timetable[i] = cyclevalue;
			while (timetable[i] > 0 && j < count)
			{
				for (navi = List.tasks.begin(); j < count; j++, navi++)
				{
					if ((!usedtable[navi->getID()-1]) && (navi->getValue() <= timetable[i]) && (t_lock[navi->getID()-1] == 0))
					{
						ss << navi->getID();
						ss << " ";
						timetable[i] -= navi->getValue();
						usedtable[navi->getID()-1] = 1;
						for (int val : navi->successors)
						{
							t_lock[val-1]--;
						}
						j = 0;
						break;
					}
					
				}
			}
			ss << "\tIdleTime: " << timetable[i];
			solution.push_back(ss.str());
			ss.str("");
			ss.clear();	
		}
		solved = 1;
		for (int i = count-1; i >= 0; i--)if (!usedtable[i]) 
		{ 
			solved = 0;	 
			std::cout << std::endl <<"<<< DISCARDED >>>" << std::endl;
			print(std::cout);
			std::cout << "<<< DISCARDED >>>" << std::endl << std::endl;
			solution.clear();
			break;
		}
	}
	cyclevalue--;
	for (int i = 0; i < count; i++)
	{
		List.locktable[i] = t_lock[i];
	}
	delete[] t_lock;
	delete[] usedtable;
	return 0;
}

int Solver::print(std::ostream & target) const
{
	int i = 1;
	target << "Opt: " << optimal << " Min: " << minimal << " Actual: " << cyclevalue << std::endl;
	for (std::string s : solution)
	{
		target << "Station: " << i << " : " << s << std::endl;
		i++;
	}
	return 0;
}

int Solver::show()
{
	std::cout << "---------------------- Solution ----------------------" << std::endl;
	List.print(std::cout);
	this->print(std::cout);
	std::cout << std::endl;
	Ev.print(std::cout);
	return 0;
}

int Solver::solve(const std::string filename)
{
	List.buildList(FH.loadData(filename));
	std::cout << "---------------------- Loaded Data ----------------------" << std::endl;
	List.print(std::cout);
	getOptimal();
	createLine();
	Ev.evaluate(timetable,cyclevalue,stations);
	return 0;
}

int Solver::solve(const std::string filename, const int stationcount)
{
	this->stations = stationcount;
	return solve(filename);
}

int Solver::save(const std::string filename) const
{
	FH.newFile(filename);
	FH.saveData(filename, List);
	FH.saveData(filename, *this);
	FH.saveData(filename, Ev);
	return 0;
}
